package controllers

import (
	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm"
	"go-crawl/crawl"
	c "go-crawl/crawl"
	"go-crawl/model"
	"net/http"
	"strconv"
)

var db = c.GetConnect()
var posts = []model.Post{}
var post = model.Post{}

func AutoCrwal() {
	posts = crawl.Crawl()
	for i := 0; i < len(posts); i++ {
		result := db.First(&post, posts[i].PostId)
		if result == nil {
			db.Create(&posts[i])
		} else {
			db.Update(&posts[i].Title, &posts[i].Link, &posts[i].Votes, &posts[i].Answers, &posts[i].Views, &posts[i].Author)
		}
	}
}
func GetAllPosts(c *gin.Context) {
	var posts = []model.Post{}
	db.Find(&posts)
	c.JSON(http.StatusOK, posts)
}
func AddPost(c *gin.Context) {
	var newPost model.Post
	if err := c.BindJSON(&newPost); err != nil {
		panic(err)
	}
	db.Save(&newPost)
	c.JSON(http.StatusCreated, newPost)
}
func DeletePost(c *gin.Context) {
	var post = model.Post{}
	id := c.Param("Id")
	IntId, _ := strconv.Atoi(id)
	db.First(&post, IntId)
	db.Delete(&post)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "deleted successfully!"})
}
func EditPostById(c *gin.Context) {
	var newpost model.Post

	id := c.Param("Id")
	IntId, _ := strconv.Atoi(id)
	db.First(&newpost, IntId)
	//if err := c.BindJSON(&newpost); err != nil {
	//	panic(err)
	//}

	//newpost.Id, _ = strconv.Atoi(c.PostForm("Id"))
	newpost.Title = c.PostForm("Title")
	newpost.Link = c.PostForm("Link")
	newpost.Votes, _ = strconv.Atoi(c.PostForm("Votes"))
	newpost.Answers, _ = strconv.Atoi(c.PostForm("Answers"))
	newpost.Views, _ = strconv.Atoi(c.PostForm("Views"))
	newpost.Author = c.PostForm("Author")

	db.Save(&newpost)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "updated successfully!"})
}
