package model

type Post struct {
	Id      int    `gorm:"primaryKey;autoIncrement"`
	PostId  int    `json:"PostId"`
	Title   string `json:"Title"`
	Link    string `json:"Link"`
	Votes   int    `json:"Votes"`
	Answers int    `json:"Answers"`
	Views   int    `json:"Views"`
	Author  string `json:"Author"`
}
