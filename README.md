# Crawl Data App Restful API 

Simple Restful API that is crawl data from https://stackoverflow.com/questions/tagged/ibm-blockchain using Golang and PostgreSQL.

## API Routes

### GET /posts

Get all posts

### POST /posts

Add new post

### PUT /posts/{id}

Edit post

### DELETE /posts/{id}

Delete post

