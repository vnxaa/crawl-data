package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jasonlvhit/gocron"
	"go-crawl/controllers"
)

func main() {
	s := gocron.NewScheduler()
	s.Every(10).Seconds().Do(controllers.AutoCrwal)
	s.Start()

	router := gin.Default()

	router.GET("/posts", controllers.GetAllPosts)
	router.POST("/posts", controllers.AddPost)
	router.PUT("/posts/:id", controllers.EditPostById)
	router.DELETE("posts/:id", controllers.DeletePost)

	router.Run("localhost:8080")

}
