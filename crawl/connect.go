package crawl

import "github.com/jinzhu/gorm"

const (
	host     = "localhost"
	port     = "5432"
	user     = "postgres"
	password = "1"
	dbname   = "crawl_data_db"
)

func GetConnect() *gorm.DB {
	db := Connect(host, port, user, password, dbname)
	return db
}
