package crawl

import (
	"fmt"
	"github.com/gocolly/colly"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	model "go-crawl/model"
	"log"
	"strconv"
)

var post = model.Post{}
var posts = []model.Post{}
var page int

func Crawl() []model.Post {

	c := colly.NewCollector()

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	c.OnHTML(".s-post-summary", func(e *colly.HTMLElement) {
		post.PostId, _ = strconv.Atoi(e.Attr("data-post-id"))
		post.Title = e.ChildText(".s-link")
		post.Link = "https://stackoverflow.com/" + e.ChildAttr(".s-post-summary--content-title .s-link", "href")
		post.Votes, _ = strconv.Atoi(e.ChildText(".s-post-summary--stats-item__emphasized .s-post-summary--stats-item-number"))
		post.Answers, _ = strconv.Atoi(e.ChildText(".s-post-summary--stats-item:nth-child(2) .s-post-summary--stats-item-number"))
		post.Views, _ = strconv.Atoi(e.ChildText(".s-post-summary--stats-item:nth-child(3) .s-post-summary--stats-item-number"))
		post.Author = e.ChildText(".s-user-card--link .flex--item")
		posts = append(posts, post)
	})
	c.OnHTML(".float-left ", func(e *colly.HTMLElement) {
		page, _ = strconv.Atoi(e.ChildText(".js-pagination-item:nth-last-child(2)"))
	})
	c.Visit("https://stackoverflow.com/questions/tagged/ibm-blockchain")
	for i := 1; i <= page; i++ {
		fullURL := fmt.Sprintf("https://stackoverflow.com/questions/tagged/ibm-blockchain?tab=newest&page=%d&pagesize=50", i)
		c.Visit(fullURL)
	}
	return posts
}

func Init(host, port, user, pass, dbname string) *gorm.DB {
	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=disable", host, port, user, pass)

	db, err := gorm.Open("postgres", connStr)

	if err != nil {
		log.Fatalln(err)
	}
	db.Exec("create database " + dbname)
	if err != nil {
		log.Fatal(err)
	}
	return db
}

func Connect(host, port, user, pass, dbname string) *gorm.DB {
	Init(host, port, user, pass, dbname)
	conn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, pass, dbname)
	db, err := gorm.Open("postgres", conn)
	if err != nil {
		log.Fatalln(err)
	}
	isExist := db.HasTable(&model.Post{})
	if isExist {
		Crawl() // crawl new data and save it to array posts
		for i := 0; i < len(posts); i++ {
			result := db.First(&post, posts[i].PostId)
			if result == nil {
				db.Create(&posts[i])
			} else {
				db.Update(&posts[i].Title, &posts[i].Link, &posts[i].Votes, &posts[i].Answers, &posts[i].Views, &posts[i].Author)
			}
		}
	} else {
		db.CreateTable(&model.Post{})
		Crawl()
		for i, _ := range posts {
			db.Create(&posts[i])
		}
	}
	return db
}
